// import { isLocalTest } from "../utils";

export const cropLink = (link: string) => {
  return link.split('/').pop() || '';
};

export default cropLink;