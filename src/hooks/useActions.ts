import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import actionsData from "../store/actions";

export const useActions = () => {
  const dispatch = useDispatch();
  return bindActionCreators(actionsData, dispatch);
};