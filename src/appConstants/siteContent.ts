// author: Victor K.
import {
  TStringsObj,
} from '../types';

const DefTitle = 'Продуктовое агенство «CYBERFORM.SYSTEMS»';

const Domain:string = 'https://cyberform.systems/';
const Mail:string = 'request@cyberform.systems';
const Phone:string = '+79771200123';

const Page:TStringsObj = {
  '/': {
    title: DefTitle,
  },
};

export const SITE_CONTENT = {
  Domain,
  Mail,
  Phone,
  Page,
};