// author: Victor K.
export const URL = {
  'ROOT': '/',
  'APPLICATION': {
    'GET': '/adv/',
    'POST': '/adv/',
  },
  'USERS': {
    'GET': '/users/',
  },
};