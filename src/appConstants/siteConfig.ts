// author: Victor K.
export const siteConfig:{
  useTestData: boolean,
  showApiErrs: boolean,
} = {
  useTestData: true,
  showApiErrs: true,
};