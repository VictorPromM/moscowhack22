import axios from 'axios';

export const isLocalTest:boolean = window.location.href.includes('http://localhost');
const prodAdress:string = 'http://3n91n33r.su/';

export const envSwitch: () => string = () => {
  return (isLocalTest ? prodAdress : process.env.REACT_APP_API_URL) as string;
};

export const apiClient = axios.create({
  baseURL: envSwitch(),
});