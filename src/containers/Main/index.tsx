import React from 'react';
import {
  BlockContent,
  Dynamic,
  History,
  Stats,
} from '../../components';
import styles from './styles.module.scss';

const Main = () => {
  return (
    <BlockContent
      title='Мониторинг эффективности рекламных компаний'
      wrapperClass={styles.main}
      contentClass={styles.mainContent}
    >
      <div className={styles.datas}>
        <Stats />
        <History />
      </div>
      <Dynamic />
    </BlockContent>
  );
};

export default Main;