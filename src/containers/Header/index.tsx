import React from 'react';
import { BlockContent } from '../../components';
import styles from './styles.module.scss';

const Header = () => {

  return (
    <BlockContent
      wrapperClass={styles.header}
      contentClass={styles.content}
    >
    </BlockContent>
  );
};

export default Header;