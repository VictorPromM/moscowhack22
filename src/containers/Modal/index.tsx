import React, { FC, useState } from 'react';
import cx from 'classnames';
import styles from './styles.module.scss';
import { Input } from '../../components';
import { TReqData } from '../../types';
import { useRequest } from '../../hooks';
import { toast } from 'react-toastify';
import { URL } from '../../appConstants';

type Props = {
  closerHandle: () => void;
}

const Modal:FC<Props> = ({
  closerHandle,
}) => {
  const { request: sendData } = useRequest({
    callback: () => {
      toast.success(`Форма успешно отправлена!`);
      closerHandle();
    },
    errback: () => {
      toast.error("Ошибка отправки формы! Пожалуйста, повторите позже.");
    }
  });
  const [ data, setData ] = useState<TReqData>({
    channelId: '',
    budget: '',
    stopTrigger: '',
    conversionLimit: '',
  });

  const handleChange = (e:any) => {
    const {name, value} = e.target;
    setData({
      ...data,
      [name]: value,
    })
  }

  const TestHandle = () => {
    sendData({
      method: 'POST',
      url: URL.APPLICATION.POST,
      data: {
        ...data,
        csrfmiddlewaretoken: 'Op5cDhMzoe207KKyxhAsK9Zkkg31uSlCx8vMxfgLGCkDebGllDBjAIC9RUc9M07w',
      },
    });
  }

  return (
    <div className={styles.modal}>
      <div className={styles.bg} onClick={() => closerHandle()} />
      <div className={styles.content}>
        <div className={styles.close} onClick={() => closerHandle()}/>

        <h3>Добавить рекламную кампанию</h3>
        <div className={styles.form}>
          <div className={cx(
            styles.fake
          )}>Telegram</div>
          <Input
            item='channelId'
            value={data.channelId}
            onChange={(e) => handleChange(e)}
            placeholder='Ссылка на контент'
          />
          <Input
            item='budget'
            value={data.budget}
            onChange={(e) => handleChange(e)}
            placeholder='Бюджет'
          />
          <Input
            item='stopTrigger'
            value={data.stopTrigger}
            onChange={(e) => handleChange(e)}
            placeholder='Стоп-триггер'
          />
          <Input
            item='conversionLimit'
            value={data.conversionLimit}
            onChange={(e) => handleChange(e)}
            placeholder='Уровень конверсии'
          />

          <div
            onClick={() => TestHandle()}
            className={styles.button}
          >
            Сохранить
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;