import React, { FC } from 'react';
import { BlockContent } from '../../components';
import styles from './styles.module.scss';

const Footer:FC = () => {

  const Copy = () => {
    const s = 2022;
    const f = new Date().getFullYear();
    return (
      <div className={styles.copy}>
        <div>&copy; {s}{f !== s && ` - ${f}`}, ООО "Кибер Форм Системс"</div>
      </div>
    )
  }
  
  return (
    <BlockContent
      id='Contacts'
      wrapperClass={styles.footer}
    >
      <Copy />
    </BlockContent>
  );
};

export default Footer;