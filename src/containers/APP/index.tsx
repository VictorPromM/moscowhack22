import { Header, Footer } from '../../containers';
import { toast } from 'react-toastify';
import AppRouter from './appRouter';

const App = () => {
  toast.configure();
  
  return (
    <>
      <Header />
      <AppRouter />
      <Footer />
    </>
  );
}

export default App;
