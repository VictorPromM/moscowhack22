import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { MainPage } from '../../pages';
import { publicRoutes } from '../../router';

const AppRouter = () => {
  return (
    <Routes>
      {publicRoutes.map((route) => 
        <Route
          key={route.path}
          path={route.path}
          element={<route.component />}
        />
      )}
      <Route
        path="*"
        element={<MainPage />}
      />
    </Routes>
  );
};

export default AppRouter;