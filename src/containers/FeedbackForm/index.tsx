// author: Victor K.
import React, { useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Input, InputFile } from '../../components';
import styles from './styles.module.scss';
import { InputErrors, URL } from '../../appConstants';
import { useRequest } from '../../hooks';
// import { TApplication } from '../../types';
import { toast } from 'react-toastify';

type TString = {[index:string]: string};

const fields:{
  nameKey: string,
  title: string,
  reqired?: boolean,
  max?: number,
}[] = [
  {
    nameKey: 'full_name',
    title: 'Имя',
    reqired: true,
    max: 100,
  },{
    nameKey: 'company',
    title: 'Название компании',
  },{
    nameKey: 'phone',
    title: 'Телефон',
    max: 12,
  },{
    nameKey: 'email',
    title: 'Email',
    reqired: true,
  },{
    nameKey: 'message',
    title: 'Опишите задачу',
  }
]

const FeedbackForm = () => {
  const { request: sendData } = useRequest({
    callback: () => {
      resetForm();
      toast.success('Данные отправлены')
    },
  });
  const [ photo, setPhoto ] = useState<'' | Blob>('');

  const validationSchema = Yup.object({
    full_name: Yup.string()
      .min(2, InputErrors.min(2))
      .max(100, InputErrors.max(100))
      .required( InputErrors.empty),
    company: Yup.string().min(2, InputErrors.min(2)),
    phone: Yup.string()
      .length(12, InputErrors.phone_format)
      .matches(/(\+7)[0-9]{10}/, {
        message: InputErrors.phone_format,
        excludeEmptyString: false,
      }),
    email: Yup.string()
      .email(InputErrors.email_format)
      .required(InputErrors.email),
    message: Yup.string().max(250, InputErrors.max(250)),
  })
  
  const {
    values,
    handleChange,
    handleSubmit,
    isValid,
    resetForm,
    errors,
   } = useFormik({
    initialValues: (() => {
      const obj:TString = {};
      fields.forEach(({nameKey}) => obj[nameKey] = '');
      obj['file'] = '';
      return obj;
    })(),
    validationSchema,
    onSubmit: (values) => {
      const data = new FormData();
      for (const key in values) { data.append(key, `${values[key]}`) }
      data.set('file', photo);
  
      sendData({
        method: 'POST',
        url: URL.APPLICATION.POST,
        data,
      });
    }
  });

  const changePhotoHandler = (file:Blob) => {
    setPhoto(file);
  }

  return (
    <form
      onSubmit={handleSubmit}
      className={styles.feedbackForm}
    >
      {fields.map(({
        nameKey,
        title,
        reqired,
        max,
      }) =>
        <Input
          key={nameKey}
          placeholder={title}
          item={nameKey}
          value={values[nameKey]}
          error={errors[nameKey]}
          onChange={handleChange}
          max={max || 250}
          reqired={!!reqired}
        />
      )}

      <InputFile
        name='photo'
        onChange={changePhotoHandler}
      />

      <div className={styles.assign}>
        <input type="checkbox"
          name="assign"
          id=""
          required
        />
      </div>

      <div>
        <button
          className={styles.submit}
          type="submit"
          disabled={!isValid}
        >
          Отправить заявку
        </button>
      </div>
    </form>
  );
};

export default FeedbackForm;