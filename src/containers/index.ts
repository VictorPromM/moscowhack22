export { default as Reqs } from './_Reqs';
export { default as FeedbackForm } from './FeedbackForm';
export { default as Footer } from './Footer';
export { default as Header } from './Header';

export { default as Modal } from './Modal';

export { default as Main } from './Main';
