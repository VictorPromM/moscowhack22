export * from './responce';

export type TelegramStats = { // этот объект приходит как результат запроса данный статы по каналам
  overage_budget: number,
  overage_budget_measure: string,
  overage_benefit: number,
  overage_benefit_measure: string,
  overage_efficiency_ratio: number,
  results: ItemStat[], // внутри массив объектов ItemStat
}

export type ItemStat = {
  id: number,
  channel_link: string,
  efficiency_ratio: number,
  budget: string,
  benefit: string, // -inf|+inf

  efficiency_good: boolean, // флаг для окраса стрелки в красный/зеленый
  lock: boolean, // флаг для замка
  cancel: boolean, // флаг для блока (знак стоп)
}

// Лог окно
export type Logs = { // этот объект приходит как результат запроса данных логов
  results: ItemLog[], // внутри массив объектов ItemLog
}

export type ItemLog = {
  efficiency_good: boolean, // флаг для окраса в красный/зеленый
  // все поля для лога
  date: string,
  time: string,
  channel: string,
  trigger: string,
  reason: string,
  state: string,
  condition: string,
}

export type TReqData = {
  // csrfmiddlewaretoken: string,
  channelId: string,
  budget: string,
  stopTrigger: string,
  conversionLimit: string,
}