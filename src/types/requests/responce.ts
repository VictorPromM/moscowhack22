export type TReqData<T = any> = {
  // Модель заявки пользователя
  count: number,
  next: string | null,
  previous: string | null,
  results: T[],
} 