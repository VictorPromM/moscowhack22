export type TSocial = {
  name: string,
  link: string,
  image: string,
  active: boolean,
};