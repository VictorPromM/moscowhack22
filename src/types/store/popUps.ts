export interface IPopUpsState {
  openAddNew: boolean,
}

export enum popUpsTypes {
  OPEN_ADD = "OPEN_ADD",
}

interface openAddNew {
  type: popUpsTypes.OPEN_ADD;
  payload: boolean;
}

export type IPopUpAction = 
  openAddNew;