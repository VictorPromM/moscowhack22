import React, { FC } from 'react';
import { SITE_CONTENT } from '../../appConstants';
import { Main } from '../../containers';

type Props = {}

const MainPage:FC<Props> = () => {
  window.scrollTo(0,0);
  document.title = SITE_CONTENT.Page['/'].title;

  return (
    <Main />
  );
};

export default MainPage;