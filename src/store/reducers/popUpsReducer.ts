import { IPopUpAction, IPopUpsState, popUpsTypes } from "../../types";

const initialState: IPopUpsState = {
  openAddNew: false,
}

export const popUpsReducer = (state: IPopUpsState = initialState, action: IPopUpAction): IPopUpsState => {
  switch (action.type) {
    case popUpsTypes.OPEN_ADD:
      return {
        ...state,
        openAddNew: action.payload,
      }
    default:
      return state
  }
}