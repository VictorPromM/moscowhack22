import { combineReducers } from "redux";
import { popUpsReducer } from "./popUpsReducer";

export const rootReducer = combineReducers({
  popUps: popUpsReducer,
})

export type RootState = ReturnType<typeof rootReducer>