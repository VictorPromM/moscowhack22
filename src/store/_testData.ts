/* * @Author: Victor K.  * @Last Modified by:   Victor K.  */
import {
  TelegramStats,
  Logs,
} from '../types';


export const TdStats:TelegramStats = {
  overage_budget: 198000,
  overage_budget_measure: 'k',
  overage_benefit: 1516810,
  overage_benefit_measure: 'k',
  overage_efficiency_ratio: 0.89,
  
  results: [
    {
      id: 1,
      channel_link: 'oper',
      efficiency_ratio: 1.5,
      budget: '98000',
      benefit: '198000',
      efficiency_good: false,
      lock: true,
      cancel: false,
    },{
      id: 2,
      channel_link: 'oper',
      efficiency_ratio: .5,
      budget: '98000',
      benefit: '198000',
      efficiency_good: false,
      lock: true,
      cancel: true,
    },{
      id: 3,
      channel_link: 'oper',
      efficiency_ratio: .9,
      budget: '98000',
      benefit: '198000',
      efficiency_good: true,
      lock: false,
      cancel: true,
    },{
      id: 4,
      channel_link: 'oper',
      efficiency_ratio: 1.1,
      budget: '98000',
      benefit: '198000',
      efficiency_good: true,
      lock: true,
      cancel: true,
    },{
      id: 5,
      channel_link: 'oper',
      efficiency_ratio: 1.0,
      budget: '98000',
      benefit: '198000',
      efficiency_good: true,
      lock: false,
      cancel: false,
    },{
      id: 6,
      channel_link: 'oper',
      efficiency_ratio: 1.5,
      budget: '98000',
      benefit: '198000',
      efficiency_good: true,
      lock: false,
      cancel: false,
    },{
      id: 7,
      channel_link: 'oper',
      efficiency_ratio: 1.5,
      budget: '98000',
      benefit: '198000',
      efficiency_good: true,
      lock: true,
      cancel: false,
    },{
      id: 8,
      channel_link: 'oper',
      efficiency_ratio: 1.5,
      budget: '98000',
      benefit: '198000',
      efficiency_good: true,
      lock: false,
      cancel: false,
    }
  ]
}

export const TdLogs:Logs = {
  results: [
    {
      efficiency_good: false,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'Сбер кредит',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    },{
      efficiency_good: false,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'fjewklfwejlkfj',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    },{
      efficiency_good: true,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'Сбер кредит',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    },{
      efficiency_good: false,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'Сбер кредит',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    },{
      efficiency_good: false,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'Сбер кредит',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    },{
      efficiency_good: true,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'Сбер кредит',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    },{
      efficiency_good: true,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'Сбер кредит',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    },{
      efficiency_good: true,
      date: '2022.06.10',
      time: '12:31:50',
      channel: '@oper.ru',
      trigger: 'Сбер кредит',
      reason: 'LinkToSomeWhere',
      state: 'Блокированно',
      condition: '',
    }
  ]
}