import React, { FC, PropsWithChildren } from 'react';
import cx from 'classnames';
import styles from './styles.module.scss';

type Props = {
  className?: string,
  childsClassName?: string,
  title?: string,
}

const RoundedBlock:FC<PropsWithChildren<Props>> = ({
  className,
  childsClassName,
  title,
  children,
}) => {
  return (
    <div className={cx( className, styles.RoundedBlock )} >
      <h3>{title}</h3>
      <div className={cx( childsClassName, styles.content )} >
        {children}
      </div>
    </div>
  );
};

export default RoundedBlock;