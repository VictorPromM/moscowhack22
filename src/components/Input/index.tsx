import React, { FC } from 'react';
import cx from 'classnames';
import styles from './styles.module.scss';

type Props = {
  item?: string,
  title?: string,
  placeholder?: string,
  value?: string,
  onChange: (e: React.ChangeEvent<any>) => void,
  error?: string,
  max?: number,
  reqired?: boolean,
}

const Input:FC<Props> = ({
  item,
  // title,
  placeholder,
  value,
  onChange = () => {},
  error,
  max = 250,
  reqired = false,
}) => {
  return (
    <div className={styles.field}>
      <input
        className={cx({[styles.filled]: !!value})}
        id={item}
        name={item}
        type={item}
        onChange={onChange}
        value={value}
        placeholder={placeholder?.concat(reqired ? '*' : '')}
        maxLength={max}
        required={reqired}
      />
      
      {error &&
        <div className={styles.error}>{error}</div>
      }
    </div>
  );
};

export default Input;