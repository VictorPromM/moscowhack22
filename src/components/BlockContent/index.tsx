import React, { FC, PropsWithChildren } from 'react';
import cx from 'classnames';
import styles from './styles.module.scss';

type Props = {
  id?: string,
  wrapperClass?: string,
  contentClass?: string,
  title?: string,
  descr?: string,
}

const BlockContent:FC<PropsWithChildren<Props>> = ({
  id,
  wrapperClass,
  contentClass,
  title,
  descr,
  children,
}) => {
  return (
    <div id={id} className={cx( wrapperClass, styles.blockWrapper )} >
      <div className={cx( contentClass, styles.blockContent )} >
        {title && 
          <h2 className={styles.blockTitle}>{title}</h2>}
        {children}
      </div>
    </div>
  );
};

export default BlockContent;