import React, { FC } from 'react';
import cx from 'classnames';
import styles from './styles.module.scss';

type Props = {
  title: string,
  value: string,
  bad?: boolean,
  warn?: boolean,
}

const BudgetItem:FC<Props> = ({
  title,
  value,
  bad = false,
  warn = false,
}) => {
  return (
    <div className={styles.budgetItem}>
      {title}
      <div className={cx({
        [styles.bad] : bad,
        [styles.warn] : warn,
      })}>
        {value}
      </div>
    </div>
  );
};

export default BudgetItem;