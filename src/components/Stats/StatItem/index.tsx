import React, { FC } from 'react';
import { ItemStat } from '../../../types';
import {ReactComponent as Arrow} from '../../../assets/images/arrow.svg';
import {ReactComponent as Lock} from '../../../assets/images/lock.svg';
import cx from 'classnames';
import styles from './styles.module.scss';

type Props = {
  item: ItemStat
}

const StatItem:FC<Props> = ({
  item,
}) => {
  const {
    channel_link,
    efficiency_ratio,
    budget,
    benefit,
    efficiency_good,
    lock,
  } = item;

  return (
    <div className={styles.statItem}>
      <Arrow className={cx({[styles.arrowDown]: !efficiency_good })}/>
      <div>{lock && <Lock />}</div>
      <div>@{channel_link}</div>
      <div className={cx({
        [styles.bad]: efficiency_ratio < 1,
        [styles.warn]: efficiency_ratio === 1,
        [styles.good]: efficiency_ratio > 1,
      })}>{efficiency_ratio}</div>
      <div>{budget}</div>
      <div className={cx({
        [styles.bad]: efficiency_ratio < 1,
        [styles.warn]: efficiency_ratio === 1,
        [styles.good]: efficiency_ratio > 1,
      })}>{benefit}</div>
    </div>
  );
};

export default StatItem;