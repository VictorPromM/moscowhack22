import React, { FC, useState } from 'react';
import { RoundedBlock, AddNewButton } from '../../components';
import { Modal } from '../../containers';
import { TdStats } from '../../store';
import BudgetItem from './BudgetItem';
import StatItem from './StatItem';
// import cx from 'classnames';
import styles from './styles.module.scss';

const Stats:FC = () => {
  const [ addNewModal, setAddModal ] = useState<boolean>(false);

  return (
    <>
      <div className={styles.stats}>
        <RoundedBlock
          title='Telegram'
          className={styles.telegramStats}
          childsClassName={styles.table}
        >
          <AddNewButton onClick={() => setAddModal(true)}/>
          
          {TdStats.results.map((item, id) =>
            <StatItem
              key={id}
              item={item}
            />
          )}
        </RoundedBlock>

        <RoundedBlock
          title='Эффективность'
          childsClassName={styles.budget}
        >
          <BudgetItem
            title='Бюджет'
            value={`${TdStats.overage_budget}`}
          />
          <BudgetItem
            title='Планируемая выгода'
            value={`${TdStats.overage_benefit}`}
            bad={TdStats.overage_efficiency_ratio < 1}
          />
          <BudgetItem
            title='Эффективность'
            value={`${TdStats.overage_efficiency_ratio}`}
            bad={TdStats.overage_efficiency_ratio < 1}
            warn={TdStats.overage_efficiency_ratio === 1}
          />

        </RoundedBlock>
      </div>

      {addNewModal &&
        <Modal
          closerHandle={() => setAddModal(false)}
        />
      }
    </>
  );
};

export default Stats;