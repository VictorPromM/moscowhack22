import React, { FC } from 'react';
import styles from './styles.module.scss';

type Props = {
  onClick: () => void,
}

const AddNewButton:FC<Props> = ({
  onClick,
}) => {
  return (
    <div
      onClick={() => onClick()}
      className={styles.add_new}
    >
      Добавить
    </div>
  );
};

export default AddNewButton;