import React, { FC } from 'react';
import { RoundedBlock } from '../../components';
import {ReactComponent as Graph} from '../../assets/images/graph.svg';
import styles from './styles.module.scss';
import Legend from './Legend';

const Dynamic:FC = () => {
  return (
    <RoundedBlock
      title='Динамика событий'
      className={styles.dynamic}
    >
      <Legend />
      <Graph />
    </RoundedBlock>
  );
};

export default Dynamic;