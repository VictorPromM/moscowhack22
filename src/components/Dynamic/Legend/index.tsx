import React from 'react';
import styles from './styles.module.scss';

const Legend = () => {
  return (
    <div className={styles.legend}>
      <div className={styles.budget}>Бюджет</div>
      <div className={styles.benefit}>Выгода</div>
      <div className={styles.scale}>Масштаб: <span>100%</span></div>
    </div>
  );
};

export default Legend;