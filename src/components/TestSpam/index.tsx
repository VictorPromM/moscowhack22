import React, { FC } from 'react';
import { toast } from 'react-toastify';
import { URL } from '../../appConstants';
import { useRequest } from '../../hooks';
import { TReqData } from '../../types';
import styles from './styles.module.scss';

type Props = {
  data: TReqData,
}

const TestSpam:FC<Props> = ({
  data
}) => {
  const { request: sendData } = useRequest({
    callback: () => {
      toast.success(`Форма успешно отправлена!`);
    },
    errback: () => {
      toast.error("Ошибка отправки формы! Пожалуйста, повторите позже.");
    }
  });

  const TestHandle = () => {
    sendData({
      method: 'POST',
      url: URL.APPLICATION.POST,
      data: {
        ...data,
        csrfmiddlewaretoken: 'Op5cDhMzoe207KKyxhAsK9Zkkg31uSlCx8vMxfgLGCkDebGllDBjAIC9RUc9M07w',
      },
    });
  }

  return (
    <div
      onClick={() => TestHandle()}
      className={styles.button}
    >
      Сохранить
    </div>
  );
};

export default TestSpam;