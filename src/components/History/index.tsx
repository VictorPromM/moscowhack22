import React, { FC } from 'react';
import { RoundedBlock } from '../../components';
import { TdLogs } from '../../store';
import HistoryItem from './HistoryItem';
// import cx from 'classnames';
import styles from './styles.module.scss';

const Stats:FC = () => {
  return (
    <RoundedBlock
      title='История событий'
      className={styles.history}
      childsClassName={styles.table}
    >
      {TdLogs.results.map((item, id) =>
        <HistoryItem
          key={id}
          item={item}
        />
      )}
    </RoundedBlock>
  );
};

export default Stats;