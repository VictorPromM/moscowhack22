import React, { FC, PropsWithChildren } from 'react';
import { ItemLog } from '../../../types';
import cx from 'classnames';
import styles from './styles.module.scss';

type Props = {
  item: ItemLog
}

const HistoryItem:FC<Props> = ({
  item,
}) => {
  const {
    efficiency_good,
    date,
    time,
    channel,
    trigger,
    reason,
    state,
  } = item;

  const Row:FC<PropsWithChildren<{title:string}>> = ({
    title,
    children
  }) => 
    <div className={styles.row}>
      <span>{title}</span>
      {children}
    </div>

  return (
    <div className={cx(
      styles.historyItem,
      {[styles.bad]: !efficiency_good},
    )}>
      <Row title='Дата'>{date}</Row>
      <Row title='Время'>{time}</Row>
      <Row title='Триггер'>{trigger}</Row>
      <Row title='Причина'>
        <a href={`//${reason}`} target="_blank" rel="noreferrer">Посмотреть</a>
      </Row>
      <Row title='Канал'>{channel}</Row>
      <Row title='Состояние'>{state}</Row>
    </div>
  );
};

export default HistoryItem;