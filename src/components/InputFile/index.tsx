// author: Victor K.
import { FC, useRef, ChangeEvent, useState } from 'react';
// import { Placeholders } from '../../appConstants';
// import { TextField } from '../../components';
import styles from './styles.module.scss';

type Props = {
  name?: string,
  value?: string,
  placeholder?: string,
  title?: string,
  error?: string,
  required?: boolean,
  onChange?: (e:any) => void,
};

const InputFile: FC<Props> = ({
  name = 'default',
  value,
  placeholder,
  title,  
  error,
  required = false,
  onChange,
}) => {
  const inputEl = useRef(null);
  const [ fileName, setFileName ] = useState<string>('');

  const onChangeLocal = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const File:File = (inputEl as any).current.files[0];
    // const File2:Blob = File;
    if (onChange) onChange(File);
    if (!!File) {
      setFileName(File.name);
    }
  }

  const format = (str:string) => {
    if (str.length > 35) {
      return `${str.substring(0,20)}...${str.substring(str.length-12)}`;
    }
    return str;
  }

  return (
    <div className={styles.field_file}>
      {<label
        htmlFor={name}
      >
        + Прикрепить файл
      </label>}     

      <input
        className={styles.input}
        type="file"
        name={name}
        ref={inputEl}
        required={required}
        accept=".png,.jpg,.jpeg,.doc,.docx,.pdf"
        onChange={(e) => onChangeLocal(e)}
      />

      {fileName &&
        // <div className={styles.fileName}>Прикреплен файл: {fileName}</div>
        <div className={styles.fileName}>Прикреплен файл: {format(fileName)}</div>
      }
    </div>
  )
}

export default InputFile;