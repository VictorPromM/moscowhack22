import { IRoute } from "../types";
import {
  MainPage,
} from "../pages";

export enum RouteNames {
  MAIN = '/',
}

export const publicRoutes: IRoute[] = [
  {
    path: RouteNames.MAIN,
    component: MainPage,
  }
]